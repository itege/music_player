import { watch } from "vue";
import { TransportController } from "./ApplicationState";

interface Visualizer {
	start: () => void;
}

const colors = ["#22c55e", "#166534"];

export class TimeBasedVisualizer implements Visualizer {
	static #sampleSize: number = 16384;
	static #fpsLimit = 60;
	static #fpsInterval = 1000 / TimeBasedVisualizer.#fpsLimit;

	#vizCtx: CanvasRenderingContext2D;
	#gradient: CanvasGradient;
	#analyserL: AnalyserNode;
	#analyserR: AnalyserNode;
	#fftSize: number = TimeBasedVisualizer.#sampleSize;
	#audBufL: Uint8Array = new Uint8Array(TimeBasedVisualizer.#sampleSize);
	#audBufR: Uint8Array = new Uint8Array(TimeBasedVisualizer.#sampleSize);
	#sliceWidth: number = 0;
	#viz: HTMLCanvasElement;
	#audioContext: AudioContext;
	#gain: GainNode;
	#previousFrame: number = Date.now();
	#requestAnimationFrame: (callback: FrameRequestCallback) => number;

	constructor(player: HTMLAudioElement, window: Window) {
		this.#requestAnimationFrame = window.requestAnimationFrame.bind(window);
		this.#viz = document.getElementById("visualization") as HTMLCanvasElement;
		this.#audioContext = new AudioContext();
		this.#gain = this.#audioContext.createGain();
		this.#gain.gain.value = TransportController.volume.value;
		watch(TransportController.volume, (val) => {
			this.#gain.gain.value = val;
		})
		
		let audioSource = this.#audioContext.createMediaElementSource(player);
		audioSource.connect(this.#gain);
		this.#gain.connect(this.#audioContext.destination);
		

		this.#analyserL = this.#audioContext.createAnalyser();
		this.#analyserR = this.#audioContext.createAnalyser();
		this.#analyserR.fftSize = this.#analyserL.fftSize = this.#fftSize;
		this.#analyserL.getByteTimeDomainData(this.#audBufL);
		this.#analyserR.getByteTimeDomainData(this.#audBufR);
		let audSplit = this.#audioContext.createChannelSplitter(2);
		audioSource.connect(audSplit);
		audSplit.connect(this.#analyserL, 0);
		audSplit.connect(this.#analyserR, 1);

		let tempCtx = this.#viz.getContext("2d");
		if (!tempCtx) {
			throw new Error("no canvas context");
		}
		this.#vizCtx = tempCtx;
		let bounds = this.#viz.getBoundingClientRect();
		this.#viz.height = bounds.height;
		this.#viz.width = bounds.width;

		this.#vizCtx.clearRect(0, 0, bounds.width, bounds.height);

		this.#sliceWidth = (bounds.width / this.#fftSize) * 8;
		this.#gradient = this.#vizCtx.createLinearGradient(0,0,0, bounds.height);
		this.#setGradient();

		window.addEventListener("resize", this.#resize);
	}

	async start() {
		this.#requestAnimationFrame((time: number) => this.#draw(time));
	}
	
	#draw(time: number): void {
		this.#requestAnimationFrame((time: number) => this.#draw(time));
		let now = Date.now();
		if (now - this.#previousFrame > TimeBasedVisualizer.#fpsInterval) {
			let bounds = this.#viz.getBoundingClientRect();
			this.#vizCtx.strokeStyle = this.#gradient;
			this.#vizCtx.lineWidth = 1;
			this.#vizCtx.clearRect(0, 0, bounds.width, bounds.height);
			let x = 0;
			this.#analyserL.getByteTimeDomainData(this.#audBufL);
			this.#analyserR.getByteTimeDomainData(this.#audBufR);

			this.#vizCtx.beginPath();
			for(let i = 0; i < this.#fftSize; i+=8) {
				this.#drawLeft(bounds, i, x);
				this.#drawRight(bounds, i, x);
				x += this.#sliceWidth;
			}
			this.#vizCtx.stroke();
			this.#previousFrame = now;
		}
	}

	#resize() {
		this.#viz = document.getElementById("visualization") as HTMLCanvasElement;

		this.#setGradient();

		let bounds = this.#viz.getBoundingClientRect();
		this.#vizCtx.clearRect(0, 0, bounds.width, bounds.height);
		this.#viz.height = bounds.height;
		this.#viz.width = bounds.width;

		this.#sliceWidth = (bounds.width / this.#fftSize) * 8;
	}

	#setGradient() {
		let bounds = this.#viz.getBoundingClientRect();
		this.#gradient = this.#vizCtx.createLinearGradient(0,0,0, bounds.height);
		this.#gradient.addColorStop(0, colors[0]);

		this.#gradient.addColorStop(.25, colors[0]);
		this.#gradient.addColorStop(.5, colors[1]);
		this.#gradient.addColorStop(.75, colors[0]);

		this.#gradient.addColorStop(1, colors[0]);
	}

	#drawLeft(bounds: DOMRect, i: number, x: number) {
		const v = this.#audBufL[i] / 128;
		if (v <= 1) {
			const y = v * bounds.height / 2;
			if (y > bounds.height / 2) {
				return;
			}
			if (i === 0) {
				this.#vizCtx.moveTo(x, y);
			}
			else {
				if (y !== bounds.height / 2) {
					this.#vizCtx.lineTo(x, y);
				}
				this.#vizCtx.moveTo(x, bounds.height / 2);
			}
		}
	}
	
	#drawRight(bounds: DOMRect, i: number, x: number) {
		let v = this.#audBufR[i] / 128;
		if (v <= 1) {
			v = 2 - v;
			const y = v * bounds.height / 2;
			if (y < bounds.height / 2) {
				return;
			}
			if (i === 0) {
				this.#vizCtx.moveTo(x, y);
			}
			else {
				if (y !== bounds.height / 2) {
					this.#vizCtx.lineTo(x, y);
				}
				this.#vizCtx.moveTo(x, bounds.height / 2);
			}
		}
	}
}